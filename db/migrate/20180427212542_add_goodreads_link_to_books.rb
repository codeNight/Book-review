class AddGoodreadsLinkToBooks < ActiveRecord::Migration
  def change
    add_column :books, :goodreads_link, :string
  end
end
