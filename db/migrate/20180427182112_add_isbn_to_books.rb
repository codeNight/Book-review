class AddIsbnToBooks < ActiveRecord::Migration
  def change
    add_column :books, :isbn, :string, unique: true 
  end
end
