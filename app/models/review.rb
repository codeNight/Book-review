class Review < ActiveRecord::Base
	validates :comment, presence: true

	belongs_to :book
	belongs_to :uesr
end
