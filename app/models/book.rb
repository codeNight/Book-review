class Book < ActiveRecord::Base
	searchkick
	belongs_to :user
	belongs_to :category
	has_many :reviews

	validates :title, :book_img, :isbn, :presence => true
	validates :isbn, length: { is: 10 }, numericality: true, uniqueness: true

	has_attached_file :book_img, :styles => { :book_index => "100x150>", :book_show => "100x150>" }#, :default_url => "/home/osama/missing.png"
  	validates_attachment_content_type :book_img, :content_type => /\Aimage\/.*\Z/

  	def self.test
		Book.select(:isbn).map { |c| c.isbn }
  	end
end

